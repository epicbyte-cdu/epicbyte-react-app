import Button from "./Button";

const TodoSection = ({ onAdd, showAddTask }) => {
  return (
    <div className="header">
      <h1>To-Do</h1>
      <Button onClick={onAdd} text={showAddTask ? "Close" : "Add"} />
    </div>
  );
};

export default TodoSection;
