import TodoSection from "../components/TodoSection";
import Tasks from "../components/Tasks";
import AddTask from "../components/AddTask";
import DoneSection from "../components/DoneSection";
import Data from "../Data.json";

import "../index.css";
import { useState } from "react";

function Todo() {
  const [showAddTask, setShowAddTask] = useState(false);
  // useState hook for manimupating the state of toggle pannel
  const [todos, setTodos] = useState([...Data["todos"]]);
  const [dones, setDones] = useState([...Data["dones"]]);
  console.log(...todos);
  console.log(...dones);

  //Add Task
  const addTask = (task) => {
    const id = todos.length !== 0 ? todos.slice(-1)[0].id + 1 : 0;
    // auto incerased ID id is the new ID for newTask, id = lastId + 1 OR 0
    // When task list is not empty, read the id of last object in the list then + 1; else assign with 0
    const newTask = { id, ...task };
    setTodos([...todos, newTask]);
  };

  // Finish task, taskd finished will be transfered to Done section
  const finishTask = (id) => {
    setTodos(todos.filter((task) => task.id !== id));
    const doneID = dones.length !== 0 ? dones.slice(-1)[0].id + 1 : 0;
    const newDone = todos.filter((task) => task.id === id);
    newDone[0].id = doneID;
    newDone[0].status = "done";
    console.log(...newDone);
    setDones([...dones, ...newDone]);
    console.log(dones);
  };

  //Delete task
  const deleteTodos = (id) => {
    setTodos(todos.filter((task) => task.id !== id));
    console.log([...todos], [...dones]);
    // setTasks manipulates the current tasks list
    // Using tasks.filter here will filter out all the tasks that has different id as the triggered one(parameter), returning the rest of the list
  };

  const deleteDones = (id) => {
    setDones(dones.filter((task) => task.id !== id));
  };

  //Toggle Reminder
  const toggleReminder = (id) => {
    setTodos(
      todos.map(
        (task) =>
          task.id === id ? { ...task, reminder: !task.reminder } : task
        // find task in tasks list and change it's reminder state
      )
    );
  };

  // Todo: finished tasks transfered to "Done"

  return (
    <div className="todo">
      <TodoSection
        onAdd={() => setShowAddTask(!showAddTask)}
        showAddTask={showAddTask}
        todos={todos}
      />
      {
        showAddTask && <AddTask onAdd={addTask} />
        // if showAddTask = true show addTask panel else hide
      }

      {todos.length > 0 ? (
        <Tasks
          tasks={todos}
          onDelete={deleteTodos}
          onToggle={toggleReminder}
          onFinish={finishTask}
        />
      ) : (
        "No Task currently, click Add to create a new task"
      )}
      <DoneSection dones={dones} />
      {dones.length > 0 ? (
        <Tasks
          tasks={dones}
          onDelete={deleteDones}
          onToggle={null}
          onFinish={finishTask}
        />
      ) : (
        "No Task currently, click Add to create a new task"
      )}
    </div>
  );
}

export default Todo;
